## Getting Started
This instructions will help you to setup this repository in your local machine to upgrade the old map definition files. :)

### Prerequisites
For setting up this project , you need `npm ` or `yarn` in your local machine. Your can setup `npm` using the following command
```
sudo apt-get install npm
```
For `yarn`,
```
sudo apt-get install yarn
```

## Installing
Download and unzip this repository in your local machine. `cd` into the repository and run `npm install` or `yarn` for installing the neccessary packages required for running the tool.

## Running the tool
After installing the neccessary packages, put the old map definition files (i.e., the files **compatible** upto **FusionCharts v3.12.2**) under the folder **old-source**, available in root, and you can run build to **convert** the files to support **FusionCharts v3.13.0** using the following command
```
npm run build
```
or
```
yarn run build
```

The converted files will be available under **dist** folder in root.

## License
This project is under the [MIT](https://opensource.org/licenses/MIT) license.