var fs = require('fs-extra'),
  cmd = require('node-cmd'),
  path = require('path'),
  source = path.resolve('./out/maps/'),
  dest = path.resolve('./source/maps/');

cmd.get('npm run-script convert',
  function(err, data, stderr){
    console.log(data);
    fs.moveSync(source, dest, { overwrite: true });
    cmd.get('npm run-script prod:build',
      function(err, data, stderr){
        console.log(data);
      }
    );
  }
);