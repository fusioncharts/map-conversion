const fs = require('fs-extra'),
  path = require('path'),
  beautify = require('js-beautify').js_beautify;

var sourceFiles,
  sourceFileContent,
  srcTemplateString,
  mapName,
  wrapperTemplateString,
  errorFiles = [],
  executeCode,
  successCounter,
  attemptCounter,
  geoDef,
  geoDefStr,
  sourceFolder = 'old-src/',
  sourceIsFile,
  outFolder = 'out/',
  convert,
  // Dummy FusionCharts class
  FusionCharts = function () {
    executeCode = arguments[0][2];
  };
FusionCharts.register = function () {
  executeCode = arguments[1][2];
};

// Parse source location
if (process.argv[2]) {
  try {
    if (fs.statSync(process.argv[2]).isFile()) {
      sourceIsFile = true;
    }
    sourceFolder = process.argv[2];
  } catch (error) {
    console.log('\x1b[31m%s\x1b[0m', 'Error: Source not found...');
  }
}
try {
  fs.statSync(sourceFolder);
} catch (error) {
  console.log('\x1b[31m%s\x1b[0m', 'Exiting process as default src/ is not found...\nMake a src folder in root...');
  process.exit();
}

// Parse destination location
if (process.argv[3]) {
  try {
    if (fs.statSync(process.argv[3]).isFile()) {
      sourceIsFile = true;
    }
    outFolder = process.argv[3];
  } catch (error) {
    console.log('\x1b[31m%s\x1b[0m', 'Error: Destination not found...');
  }
}
// Clear error log file if any
if (fs.existsSync(outFolder + 'list-of-failed-files.txt')) {
  fs.unlinkSync(outFolder + 'list-of-failed-files.txt');
}
// Create out folder if not present
if (outFolder === 'out/') {
  try {
    fs.statSync(outFolder);
  } catch (error) {
    fs.mkdirSync(outFolder);
  }
}
// Empty out folder contents
fs.emptyDirSync(outFolder);
fs.mkdirSync(outFolder + 'maps/');
fs.mkdirSync(outFolder + 'wrappers/');

console.log('\x1b[33m%s\x1b[0m', '\nSource: ' + sourceFolder);
console.log('\x1b[33m%s\x1b[0m', 'Destination: ' + outFolder);
console.log('\x1b[36m%s\x1b[0m', '\nTask initiated successfully!');
console.log('\x1b[33m%s\x1b[0m', '\nConverting files...');
console.log('\nFile Name - Status');
console.log('-------------------\n');

// Converts map files
convert = function (sourceFile) {
  geoDef = undefined;
  executeCode = undefined;
  // Checks whether it's a '.js ' file
  if (sourceFile.endsWith('.js')) {
    attemptCounter++;
    // Read individual file
    sourceFileContent = fs.readFileSync(sourceIsFile ? sourceFolder : (sourceFolder + sourceFile), 'utf8');
    mapName = sourceFile.split('.')[1];

    // Source template
    srcTemplateString = `/**!
 * @license FusionCharts JavaScript Library
 * Copyright FusionCharts Technologies LLP
 * License Information at <http://www.fusioncharts.com/license>
 *
 * @author FusionCharts Technologies LLP
 * @meta package_map_pack
 __MAP_ID__
 */

let M = 'M', // SVG MoveTo
  L = 'L', // SVG LineTo
  Z = 'Z', // SVG ClosePath
  Q = 'Q', // SVG Quadratic Beizer
  LFT = 'left',
  RGT = 'right',
  CEN = 'center',
  MID = 'middle',
  TOP = 'top',
  BTM = 'bottom',
  geodefinitions = __GEO_DEF__;

export default {
  extension: geodefinitions,
  name: '__MAP_NAME__',
  type: 'maps'
};`;
    srcTemplateString = srcTemplateString.replace(/__MAP_ID__/g, sourceFileContent.match(/\* @id fusionmaps.*/g)[0]);
    // Wrapper template
    wrapperTemplateString = `import __MAP_NAME__ from '../maps/fusioncharts.__MAP_NAME__';

FusionCharts.addDep(__MAP_NAME__);
FusionCharts.addFile('fusioncharts.__MAP_NAME__.js');`;
    wrapperTemplateString = wrapperTemplateString.replace(/__MAP_NAME__/g, mapName);

    // Change code content to make it run without error
    sourceFileContent = sourceFileContent.replace(/geodefinitions =/g, 'geoDef = geodefinitions =')
      .replace('global.hcLib,', '{},')
      .replace('lib.chartAPI,', '{},')
      .replace('lib.moduleCmdQueue,', '{maps: []},')
      .replace('lib.injectModuleDependency,', 'function () {},')
      .replace(/window/g, 'global');
    // Run the file content as js
    eval(sourceFileContent);
    (typeof module.exports === 'function') && module.exports(FusionCharts);
    executeCode();

    if (!JSON.stringify(geoDef)) {
      console.log('\x1b[31m%s\x1b[0m', attemptCounter + '. ' + sourceFile + ' - Fail');
      errorFiles.push(sourceFile);
      return;
    }
    geoDefStr = JSON.stringify(geoDef)
      .replace(/"M"/g, 'M')
      .replace(/"L"/g, 'L')
      .replace(/"Z"/g, 'Z')
      .replace(/"Q"/g, 'Q')
      .replace(/"left"/g, 'LFT')
      .replace(/"right"/g, 'RGT')
      .replace(/"center"/g, 'CEN')
      .replace(/"middle"/g, 'MID')
      .replace(/"top"/g, 'TOP')
      .replace(/"bottom"/g, 'BTM');

    srcTemplateString = srcTemplateString.replace(/__GEO_DEF__/g, beautify(geoDefStr, {
      'indent_size': 2
    }));
    srcTemplateString = srcTemplateString.replace(/__MAP_NAME__/g, mapName);

    // Write the final converted es6 code to out directory
    fs.writeFileSync(outFolder + 'maps/' + sourceFile, srcTemplateString, 'utf8');
    fs.writeFileSync(outFolder + 'wrappers/' + sourceFile, wrapperTemplateString, 'utf8');
    successCounter++;
    console.log('\x1b[32m%s\x1b[0m', attemptCounter + '. ' + sourceFile + ' - Success');
  }
};

successCounter = 0;
attemptCounter = 0;

if (sourceIsFile) {
  convert(path.basename(sourceFolder));
} else {
  // Read files from source source directory
  sourceFiles = fs.readdirSync(sourceFolder);
  // Traverse files in source source directory
  sourceFiles.forEach(function (sourceFile) {
    convert(sourceFile);
  });
}

console.log('\x1b[36m%s\x1b[0m', '\nTask Completed! Attempted ' + attemptCounter + (attemptCounter < 2 ? ' file' : ' files') + '\n');
(successCounter > 0) && console.log('\x1b[32m%s\x1b[0m', '  - Succeded to convert ' + successCounter +
 (successCounter < 2 ? ' file' : ' files') + ' (Success Rate: ' + ((successCounter / attemptCounter) * 100).toFixed(2) + '%)\n');
(errorFiles.length > 0) && console.log('\x1b[31m%s\x1b[0m', '  - Failed to convert ' + errorFiles.length +
 (errorFiles.length < 2 ? ' file' : ' files') + ' (Failure Rate: ' + ((errorFiles.length / attemptCounter) * 100).toFixed(2) + '%)\n');

if (errorFiles.length > 0) {
  fs.writeFileSync(outFolder + 'list-of-failed-files.txt', 'List of Failed Files:\n*********************\n' +
    JSON.stringify(errorFiles, null, 1).replace('[', ' ').replace(']', ''), 'utf8');
    console.log('\x1b[36m%s\x1b[0m', 'Failure report generated! (' + outFolder + 'list-of-failed-files.txt)\n');
}
